from django.urls import path

from . import views


app_name = 'nucleo'

#agregamos las direcciones de nuestros template
urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
 
]