from django.shortcuts import render

# Create your views here.
def index(request):
    return render (request,'nucleo/index.html')

#creamos para la direccion "about"
def about(request):
    return render(request,'nucleo/about.html')